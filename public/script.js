function loadSaved() {
  var savedObject = JSON.parse(localStorage.getItem('picole'));
  
  if( !savedObject ) {
    return;
  }
  
  var savedValues = Object.values(savedObject);
  var savedKeys = Object.keys(savedObject);
  
  var split, calendarDay;
  for(var i = 0; i < savedValues.length; i++) {
    split = savedKeys[i].split(':');
    document.querySelector('.picole tbody tr:nth-child(' + split[1] + 'n) td:nth-child(' + split[0] + 'n)').dataset.color = savedValues[i];
  }
}

window.addEventListener('load', function(){
  loadSaved();
  
  var calendar = document.querySelector('.picole tbody');
  var tdColor, saved;
  
  calendar.addEventListener('click', function(e) {
    if(e.target.classList.contains('dis') ||
      e.target.tagName != 'TD' ||
      e.target == e.target.parentNode.firstChild) {
      return false;
    }
    
    tdColor = parseInt(e.target.dataset.color);
    
    if(isNaN(tdColor)) {
      e.target.dataset.color = 1;
      tdColor = 1;
    }
    else if(5 == tdColor) {
      e.target.dataset.color = 0;
      tdColor = 0;
    }
    else {
      tdColor += 1;
      e.target.dataset.color = tdColor;
    }
    
    iDay = 0;
    for(var i = 0; i < 50; i++) {
      if(e.target == e.target.parentNode.querySelector('td:nth-child(' + i +')')) {
        iDay = i;
      }
    }
    
    iMonth = 0;
    for(var i = 0; i < 50; i++) { // I fucked up Month <> Day
      if(e.target.parentNode == e.target.parentNode.parentNode.querySelector('tr:nth-child(' + i +')')) {
        iMonth = i;
      }
    }
    
    saved = localStorage.getItem('picole');

    if( null == saved || 'undefined' == saved || '' == saved) {
      saved = new Object();
    }
    else {
      saved = JSON.parse(saved);
    }
    
    saved[iDay + ':' + iMonth] = tdColor;
    localStorage.setItem('picole', JSON.stringify(saved));
  });
});